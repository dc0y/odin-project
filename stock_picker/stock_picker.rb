# frozen_string_literal: false

def stock_picker(prices)
  profits = []
  prices.each do |price|
    index = prices.index(price)
    time_left = prices[index..-1]
    profit = (time_left.max - price)
    profits.push(profit)
  end
  buy = profits.index(profits.max)
  time_left = prices[buy..-1]
  sell = prices.index(time_left.max)
  if buy.zero? && sell.zero?
    puts 'Do not buy right now'
  else
    p [buy, sell]
  end
end

stock_picker([17, 3, 6, 9, 15, 8, 6, 1, 10])